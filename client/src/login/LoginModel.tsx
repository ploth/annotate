import { LoginPostResponse } from '../api/Login';
import { Option } from '../util/Types';

export interface OwnProps {

}

export interface State {
  clientID: Option<string>;
  code: string;
  login: Option<LoginPostResponse>;
}

export interface Actions {
  onLoad: () => void;
  onCode: (code: String) => void;
  onSignOut: () => void;
}

export interface Props extends OwnProps {
  state: State;
}
