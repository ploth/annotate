import { Button } from 'react-bootstrap';
import * as React from 'react';
import { Actions, Props } from './LoginModel';
import { Option } from '../util/Types';
import { Lifecycle } from '../Lifecycle';

const getParameter = (param: string): Option<string> => {
  return Option.of(
    window.location.search
      .substr(1)
      .split('&')
      .map(pair => pair.split('='))
      .find(pair => pair[0] === param))
    .map(pair => pair[1]);
};

export const Component = (props: Props & Actions) => {
  return (
    <form>
      <Lifecycle
        componentDidMount={() => {
          getParameter('code')
            .filter(c => props.state.login.isEmpty())
            .filter(c => c !== props.state.code)
            .map(code => props.onCode(code));
          props.onLoad();
        }}
      />
      {props.state.login
        .map(login => (
          <Button
            onClick={() => props.onSignOut()}
          >
            ({login.login}) Logout
          </Button>
        ))
        .or(() => props.state.clientID
          .map(authorizeUrl)
          .map(url => (
            <Button
              onClick={() => {
                window.location.href = url;
              }}
            >
              Login with Github
            </Button>
          ))
        )
        .get(() => <div/>)
      }
    </form>
  );
};

const authorizeUrl = (clientId: string): string => {
  return `https://github.com/login/oauth/authorize?client_id=${clientId}`;
};
