import { ReducerFactory } from './util/Reducers';
import { connect, Dispatch } from 'react-redux';
import { RootState } from './index';
import { Actions, OwnProps, Props, State, KeyboardLayout } from './AppModel';
import { Component } from './AppView';
import { Option } from './util/Types';

const actions = new ReducerFactory<State, OwnProps>();

const UpdateKeyboard = actions.createAction<string>((state, action) => {
  localStorage.setItem('keyboard', action.value);
  return {
    ...state,
    keyboard: KeyboardLayout[action.value]
  };
});

const initialState: State = {
  keyboard: Option
    .of(localStorage.getItem('keyboard'))
    .map(s => KeyboardLayout[s])
    .get( () => KeyboardLayout.DEFAULT )
};

export const AppReducer = actions.build(initialState);

export const App = connect(
  (rootState: RootState, ownProps: OwnProps): Props => {
    return {
      ...ownProps,
      authToken: rootState.login.login.map(l => l.accessToken),
      state: {
        ...rootState.app
      }
    };
  },
  (dispatch: Dispatch<{}>, props: OwnProps): Actions => {
    return {
      onUpdateKeyboard: UpdateKeyboard.create(dispatch, props)
    };
  }
)(Component);
