import * as React from 'react';
import { AnnotateImage } from './annotation/AnnotationController';
import { Button, ButtonGroup, Col, Grid, Navbar, Row } from 'react-bootstrap';
import { Actions, KeyboardLayout, Props } from './AppModel';
import { Score } from './scores/ScoreController';
import { Readme } from './readme/ReadmeView';
import { LoginButton } from './login/LoginController';

export const Component = (props: Props & Actions) => {
  return (
    <div>
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            annotate!
          </Navbar.Brand>
          <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
          <Navbar.Form pullRight={true}>
            <ButtonGroup>
              <Button disabled={true}>
                Keyboard:
              </Button>
              {Object.keys(KeyboardLayout)
                .filter(k => isNaN(Number(k)))
                .map((key, index) => (
                  <Button
                    key={index}
                    active={index.toString() === props.state.keyboard.toString()}
                    onClick={() => props.onUpdateKeyboard(key)}
                  >
                    {key}
                  </Button>
                ))}
            </ButtonGroup>
          </Navbar.Form>
          <Navbar.Form pullRight={true}>
            <LoginButton/>
          </Navbar.Form>
        </Navbar.Collapse>
      </Navbar>
      <Grid>
        <AnnotateImage
          authToken={props.authToken}
          keyboard={props.state.keyboard}
        />
        <Readme keyboard={props.state.keyboard}/>
        <Row>
          <Col xs={12} md={12}>
            <Score/>
          </Col>
        </Row>
      </Grid>
    </div>
  );
};
