export interface LabelsV2 {
  images: string[];
  boxes: string[];
}
