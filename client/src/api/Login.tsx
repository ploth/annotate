export interface LoginPostRequest {
  code: string
}

export interface LoginPostResponse {
  accessToken: string,
  login: string
}

export interface ClientIDGet {
  id: string
}
