export interface Stats {
  labeled: number;
  remaining: number;
}
