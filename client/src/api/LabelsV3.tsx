export interface LabelsV3 {
  images: string[];
  boxes: string[];
  lines: string[];
}
