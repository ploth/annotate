import { Option } from './util/Types';

export enum KeyboardLayout {
  DEFAULT,
  FANCY
}

export interface OwnProps {
  authToken: Option<string>;
}

export interface State {
  keyboard: KeyboardLayout;
}

export interface Actions {
  onUpdateKeyboard: (e: string) => void;
}

export interface Props extends OwnProps {
  state: State;
}
