import { GenericPoint } from '../api/Point';
import * as React from 'react';

export const Rect = (props: {
  start: GenericPoint<string>;
  size: GenericPoint<string>;
  content?: string;
  color?: string;
  background?: string
}) => {
  return (
    <g>
      <rect
        x={props.start.x}
        y={props.start.y}
        width={props.size.x}
        height={props.size.y}
        style={{
          stroke: props.color ? props.color : 'black',
          strokeWidth: '2',
          fill: 'none'
        }}
      />
      {props.content
        ? <svg
          x={props.start.x}
          y={props.start.y}
          width={props.size.x}
          height={props.size.y}
        >
          <text
            fontWeight="bold"
            fill={props.color ? props.color : 'black'}
            x={0}
            y="1em"
          >
            {props.content}
          </text>
        </svg>
        : null
      }
    </g>
  );
};
