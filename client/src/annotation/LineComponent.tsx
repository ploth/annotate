import { Point } from '../api/Point';
import * as React from 'react';

export const Line = (props: { p1: Point, p2: Point, color?: string, label?: string }) => {
  return (
    <g>
      <line
        x1={`${props.p1.x * 100}%`}
        y1={`${props.p1.y * 100}%`}
        x2={`${props.p2.x * 100}%`}
        y2={`${props.p2.y * 100}%`}
        style={{
          stroke: props.color ? props.color : 'black',
          strokeWidth: '2'
        }}
      />
      {props.label
        ? <text
          x={`${props.p1.x * 100}%`}
          y={`${props.p1.y * 100}%`}
          fontWeight="bold"
          fill={props.color ? props.color : 'black'}
        >
          {props.label}
        </text>
        : null
      }
    </g>
  );
};
