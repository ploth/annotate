import * as React from 'react';
import { Rect } from './RectComponent';
import { Point } from '../api/Point';
import { ToPercent } from './AnnotationView';
import { HULKS_PINK } from './AnnotationModel';

export const RectSelector = (props: { p1: Point, p2: Point, stateStart: Point, stateSize: Point, label: string }) => {
  const p1p = ToPercent(props.stateStart);
  let sizep = ToPercent(props.stateSize);
  return (
    <g>
      <Rect
        start={p1p}
        size={sizep}
        content={props.label}
        color={HULKS_PINK}
      />
      <svg
        x={p1p.x}
        y={p1p.y}
        width={sizep.x}
        height={sizep.y}
      >
        <Rect start={{x: '0', y: '0'}} size={{x: '2%', y: '2%'}} color={HULKS_PINK}/>
        <Rect start={{x: '98%', y: '98%'}} size={{x: '2%', y: '2%'}} color={HULKS_PINK}/>
      </svg>
    </g>
  );
};
