import { Actions, CurrentMove, OwnProps, Props, State } from './AnnotationModel';
import { Action, ReducerFactory } from '../util/Reducers';
import * as Reacts from '../util/Reacts';
import { Point, Points } from '../api/Point';
import { connect, Dispatch } from 'react-redux';
import { RootState } from '../index';
import { Component } from './AnnotationView';
import { Box, Label, Line, PostImage } from '../api/Image';
import axios from 'axios';
import { Option } from '../util/Types';
import { LabelsV3 } from '../api/LabelsV3';

const reducer = new ReducerFactory<State, Props>();

const getKeyByValue = (object: {}, value: string | undefined): string | null => {
  let keys = Object.keys(object);
  for (let i = 0; i < keys.length; i++) {
    if (object[keys[i]].indexOf(value) >= 0) {
      return keys[i];
    }
  }
  return null;
};

const ChangeLabel = reducer.createAction<{ current?: string, image?: string }>((s, a) => {
  const labelType = getKeyByValue({lines: s.labels.lines, boxes: s.labels.boxes}, a.value.current);
  return ({
    ...s,
    labelType: labelType ? labelType : s.labelType,
    label: {
      current: Option.of(a.value.current).get(() => s.label.current),
      image: Option.of(a.value.image).get(() => s.label.image)
    }
  });
});

const ChangeExistingLabel = reducer.createAction<{ id: number, label: string }>((s, a) => {
  if (s.labelType === 'boxes') {
    return {
      ...s,
      box: s.box.map((b, i): Box => {
        if (i === a.value.id) {
          return {
            ...b,
            label: a.value.label
          };
        }
        return b;
      })
    };
  } else {
    return {
      ...s,
      lines: s.lines.map((b, i): Line => {
        if (i === a.value.id) {
          return {
            ...b,
            label: a.value.label
          };
        }
        return b;
      })
    };
  }
});

const MouseDown = reducer.createAction<React.MouseEvent<{}>>((s, a) => {
  const p = Reacts.fromMouseEvent(Reacts.fromMouseClick(a.value), a.value);
  return HandleStart(s, p);
});

const TouchStart = reducer.createAction<React.TouchEvent<{}>>((s, a) => {
  a.value.preventDefault();
  const p = Reacts.fromMouseEvent(Reacts.fromTouchClick(a.value), a.value);
  return HandleStart(s, p);
});

const HandleStart = (s: State, p: Point) => {
  if (s.initial) {
    return {
      ...s,
      initial: false,
      currentMove: CurrentMove.P2,
      p1: p,
      p2: p
    };
  }
  const d1 = Points.squareAbs(Points.minus(s.p1, p));
  const d2 = Points.squareAbs(Points.minus(s.p2, p));
  if (d2 < d1) {
    return {
      ...s, currentMove: CurrentMove.P2, p2: p
    };
  } else {
    return {
      ...s, currentMove: CurrentMove.P1, p1: p
    };
  }
};

const MouseMove = reducer.createAction<React.MouseEvent<{}>>((s, a) => {
  if (s.currentMove === CurrentMove.NONE) {
    return s;
  }
  const p = Reacts.fromMouseEvent(Reacts.fromMouseClick(a.value), a.value);
  if (s.currentMove === CurrentMove.P1) {
    return {
      ...s, p1: p
    };
  }
  return {
    ...s, p2: p
  };
});

const TouchMove = reducer.createAction<React.TouchEvent<{}>>((s, a) => {
  a.value.preventDefault();
  if (s.currentMove === CurrentMove.NONE) {
    return s;
  }
  const p = Reacts.fromMouseEvent(Reacts.fromTouchClick(a.value), a.value);
  if (s.currentMove === CurrentMove.P1) {
    return {
      ...s, p1: p
    };
  }
  return {
    ...s, p2: p
  };
});

const ArrowMove = reducer.createAction<{ e: KeyboardEvent, d: Point }>((s, a) => {
  let factor = a.value.e.shiftKey ? 10 : 1;
  const diff = Points.times(a.value.d, factor);
  const min = {x: 0, y: 0};
  const max = {x: 1, y: 1};
  if (a.value.e.ctrlKey) {
    return {
      ...s,
      p1: Points.cap(Points.plus(s.p1, diff), min, max),
      initial: false,
      arrowUsed: true
    };
  } else {
    return {
      ...s,
      p2: Points.cap(Points.plus(s.p2, diff), min, max),
      initial: false,
      arrowUsed: true
    };
  }
});

const MouseUp = reducer.createAction<React.MouseEvent<{}>>((s, a) => {
  if (s.currentMove === CurrentMove.NONE) {
    return s;
  }
  const p = Reacts.fromMouseEvent(Reacts.fromMouseClick(a.value), a.value);
  if (s.currentMove === CurrentMove.P1) {
    return {
      ...s, currentMove: CurrentMove.NONE, p1: p
    };
  } else {
    return {
      ...s, currentMove: CurrentMove.NONE, p2: p
    };
  }
});

const TouchEnd = reducer.createAction<React.TouchEvent<{}>>((s, a) => {
  a.value.preventDefault();
  if (s.currentMove === CurrentMove.NONE) {
    return s;
  }
  if (s.currentMove === CurrentMove.P1) {
    return {
      ...s, currentMove: CurrentMove.NONE
    };
  } else {
    return {
      ...s, currentMove: CurrentMove.NONE
    };
  }
});

const CreateLabel = reducer.createVoidAction((state, action) => {
  if (state.initial) {
    return state;
  }
  if (state.labelType === 'boxes') {
    const box = makeBox(state.label.current, state.p1, state.p2);
    return {
      ...state,
      box: [...state.box, box]
    };
  }
  if (state.labelType === 'lines') {
    const line: Line = {
      label: state.label.current,
      start: state.p1,
      end: state.p2
    };
    return {
      ...state,
      lines: [...state.lines, line]
    };
  }
  return state;
});

function makeBox(label: string, p1: Point, p2: Point): Box {
  const start = Points.reduce(p1, p2, Math.min);
  const size = Points.apply(Points.minus(p2, p1), Math.abs);
  return {
    label: label, start: start, size: size,
  };
}

const DeleteBox = reducer.createAction<number>(
  (state, boxIndex) => ({
    ...state,
    box: state.box.filter((b, i) => i !== boxIndex.value)
  })
);

const DeleteLine = reducer.createAction<number>(
  (state, boxIndex) => ({
    ...state,
    lines: state.lines.filter((b, i) => i !== boxIndex.value)
  })
);

const SaveImage = reducer.createVoidAction(
  (state, action) => {
    const auth = action.props.authToken
      .map(a => ({
        authToken: a
      } as {}))
      .get(() => ({}));
    const data: PostImage = {
      ...auth,
      label: {
        url: state.image,
        label: state.label.image,
        boxes: state.box,
        lines: state.lines
      }
    };
    axios.post('/v2/image', data).then(() => {
      Reset.create(action.dispatch, action.props)();
      AddPreviousImage.create(action.dispatch, action.props)(state.image);
      LoadImage.create(action.dispatch, action.props)();
      LoadLabels.create(action.dispatch, action.props)();
    });
    return state;
  }
);

const AddPreviousImage = reducer.createAction<string>((state, action) => {
  let previousImages = state.previousImages;
  previousImages.unshift(action.value);
  return {
    ...state,
    previousImages: previousImages.slice(0, 10)
  };
});

const LoadLabels = reducer.createVoidAction(
  (state, action) => {
    axios
      .get('/v3/labels')
      .then(response => response.data as LabelsV3)
      .then(labels => UpdateLabels.create(action.dispatch, action.props)(labels));
    return state;
  });

const LoadImage = reducer.createVoidAction(
  (state, action) => {
    axios
      .get('/v2/image')
      .then(response => response.data as Label)
      .then(label => UpdateImage.create(action.dispatch, action.props)(label));
    return state;
  }
);

const LoadSpecificImage = reducer.createAction<string>(
  (state, action) => {
    const base64ImageUrl = btoa(action.value);
    axios
      .get(`/v2/image?image=${base64ImageUrl}`)
      .then(response => response.data as Label)
      .then(label => UpdateImage.create(action.dispatch, action.props)(label));
    return state;
  }
);

const ChangeLoadImage = reducer.createAction<string>(
  (state, action) => ({
    ...state,
    loadImage: action.value
  })
);

const UpdateImage = reducer.createAction<Label>(
  (state, action) => {
    return {
      ...state,
      image: action.value.url,
      loadImage: action.value.url,
      label: {
        ...state.label,
        ...Option.of(action.value.label)
          .map<{}>(l => ({image: l}))
          .get(() => ({}))
      },
      box: action.value.boxes,
      lines: Option.of(action.value.lines).get(() => [])
    };
  }
);

const UpdateLabels = reducer.createAction<LabelsV3>(
  (state, action) => ({
    ...state,
    label: {
      current: state.label.current === '' ? action.value.boxes[0] : state.label.current,
      image: state.label.image === '' ? action.value.images[0] : state.label.image
    },
    labels: action.value,
    labelType: 'boxes',
    labelTypes: ['boxes', 'lines']
  })
);

const Reset = reducer.createVoidAction((s) => initialState);

const ResetSelect = reducer.createVoidAction(s => ({
  ...s,
  ...initialSelectState
}));

const initialSelectState = {
  initial: true,
  arrowUsed: false,
  offset: {x: 0, y: 0},
  p1: {x: 0, y: 0},
  p2: {x: 1, y: 1},
  currentMove: CurrentMove.NONE,
};

const initialState: State = {
  ...initialSelectState,
  label: {
    current: '',
    image: ''
  },
  labels: {
    images: [],
    boxes: [],
    lines: []
  },
  labelTypes: [],
  labelType: '',
  box: [],
  lines: [],
  image: '',
  previousImages: [],
  loadImage: ''
};

export const Reducer = reducer.build(initialState);

export const AnnotateImage = connect(
  (state: RootState, ownProps: OwnProps) => {
    return ({
      ...ownProps,
      state: state.annotation
    });
  },
  (dispatch: Dispatch<Action<{}, {}>>, props: Props): Actions => ({
    onMouseDown: MouseDown.create(dispatch, props),
    onMouseMove: MouseMove.create(dispatch, props),
    onMouseUp: MouseUp.create(dispatch, props),
    onTouchStart: TouchStart.create(dispatch, props),
    onTouchMove: TouchMove.create(dispatch, props),
    onTouchEnd: TouchEnd.create(dispatch, props),
    onArrow: ArrowMove.create(dispatch, props),
    onChangeLabel: ChangeLabel.create(dispatch, props),
    onCreateLabel: CreateLabel.create(dispatch, props),
    onDeleteBox: DeleteBox.create(dispatch, props),
    onDeleteLine: DeleteLine.create(dispatch, props),
    onSave: SaveImage.create(dispatch, props),
    onChangeLoadImage: ChangeLoadImage.create(dispatch, props),
    onLoadSpecificImage: LoadSpecificImage.create(dispatch, props),
    onLoadImage: LoadImage.create(dispatch, props),
    onLoadLabels: LoadLabels.create(dispatch, props),
    onReset: Reset.create(dispatch, props),
    onResetSelect: ResetSelect.create(dispatch, props),
    onChangeExistingLabel: ChangeExistingLabel.create(dispatch, props)

  }))(Component);
