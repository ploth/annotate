import { Point } from '../api/Point';
import { Box, Line } from '../api/Image';
import { Option } from '../util/Types';
import { LabelsV3 } from '../api/LabelsV3';
import { KeyboardLayout } from '../AppModel';

export enum CurrentMove {
  P1, P2, NONE
}

export const HULKS_PINK = 'rgb(254, 0, 123)';

export const ObjectTypes = [
  '#AA0000',
  '#00AAAA',
];

export const ObjectColors = [
  '#AA0000',
  '#00AAAA',
  '#AA00AA',
  '#0000AA',
  '#000000',
  '#AAAA00',
  '#AAAAAA',
  '#00AA00'
];

export const ImageColors = [
  '#EEEEEE',
  '#00AAAA',
  '#AA0000',
  '#AAAA00',
  '#AA00AA',
  '#00AA00',
  '#0000AA',
  '#000000',
];

export interface State {
  initial: boolean;
  offset: Point;

  labelTypes: string[];
  labelType: string;
  p1: Point;
  p2: Point;
  currentMove: CurrentMove;
  arrowUsed: boolean;
  label: {
    current: string;
    image: string;
  };
  labels: LabelsV3;
  image: string;
  loadImage: string;
  previousImages: string[];
  box: Box[];
  lines: Line[];
}

export interface OwnProps {
  authToken: Option<string>;
  keyboard: KeyboardLayout;
}

export interface Actions {
  onMouseDown: (e: React.MouseEvent<{}>) => void;
  onMouseMove: (e: React.MouseEvent<{}>) => void;
  onMouseUp: (e: React.MouseEvent<{}>) => void;
  onTouchStart: (e: React.TouchEvent<{}>) => void;
  onTouchMove: (e: React.TouchEvent<{}>) => void;
  onTouchEnd: (e: React.TouchEvent<{}>) => void;
  onArrow: (a: { e: KeyboardEvent, d: Point }) => void;
  onChangeLabel: (l: { current?: string, image?: string }) => void;
  onChangeExistingLabel: (v: { id: number, label: string }) => void;
  onCreateLabel: () => void;
  onDeleteBox: (boxIndex: number) => void;
  onDeleteLine: (i: number) => void;
  onSave: () => void;
  onChangeLoadImage: (v: string) => void;
  onLoadSpecificImage: (v: string) => void;
  onLoadImage: (v: {}) => void;
  onLoadLabels: (v: {}) => void;
  onReset: () => void;
  onResetSelect: () => void;
}

export interface Props extends OwnProps {
  state: State;
}
