import * as React from 'react';
import { isUndefined } from 'util';

export class KeyBinding extends React.Component {
  props: {
    code: string,
    ctrl?: boolean,
    shift?: boolean,
    onKey: (e: KeyboardEvent) => void,
    keepDefault?: boolean
  };
  event = (e: KeyboardEvent) => this.handleKey(e);

  componentDidMount() {
    document.addEventListener('keydown', this.event);
  }

  componentWillUnMount() {
    document.removeEventListener('keydown', this.event);
  }

  handleKey(e: KeyboardEvent) {
    if (e.code !== this.props.code) {
      return;
    }
    if (checkMetaKey(e.ctrlKey, this.props.ctrl)) {
      return;
    }
    if (checkMetaKey(e.shiftKey, this.props.shift)) {
      return;
    }
    if (!this.props.keepDefault) {
      e.preventDefault();
    }
    this.props.onKey(e);
  }

  render() {
    return <div/>;
  }
}

function checkMetaKey(meta: boolean, predicate?: boolean): boolean {
  return !isUndefined(predicate) && predicate !== meta;
}
