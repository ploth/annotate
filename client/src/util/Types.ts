export class Stream<T> {
  private iterator: Iterator<T>;
  private filterer: (s: T) => boolean;

  public static of<S>(i: Iterator<S>): Stream<S> {
    return new Stream(i, e => true);
  }

  public static ofMap<V>(map: { [k: string]: V }): Stream<{ key: string, value: V }> {
    return Stream.of(iterateMap(map));
  }

  public static ofValues<S>(map: { [k: string]: S }): Stream<S> {
    return Stream.of(iterate(map));
  }

  public map<V>(f: (t: T) => V): Stream<V> {
    return new Stream(this.apply(f), e => true);
  }

  public filter(f: (s: T) => boolean): Stream<T> {
    this.filterer = f;
    return this;
  }

  public fold<V>(reducer: ((v: V, t: T) => V), initial: V): V {
    let result = initial;
    let it = this.iterator.next();
    while (!it.done) {
      result = reducer(result, it.value);
      it = this.iterator.next();
    }
    return result;
  }

  public toIterator(): Iterator<T> {
    return this.iterator;
  }

  public toArray(): T[] {
    let result = [];
    let n = this.iterator.next();
    while (!n.done) {
      result.push(n.value);
      n = this.iterator.next();
    }
    return result;
  }

  private constructor(iterator: Iterator<T>, filter: (s: T) => boolean) {
    this.iterator = iterator;
    this.filterer = filter;
  }

  private * apply<V>(f: (s: T) => V): Iterator<V> {
    let n = this.iterator.next();
    while (!n.done) {
      if (this.filterer(n.value)) {
        yield f(n.value);
      }
      n = this.iterator.next();
    }
  }
}

export function* Range(start: number, end: number, step: number = 1): Iterator<number> {
  for (let i = start; i < end; i += step) {
    yield i;
  }
}

export class Option<T> {
  private t?: T;

  static of<T>(t?: T | null): Option<T> {
    if (t) {
      return new Option(t);
    }
    return new Option();
  }

  constructor(t?: T) {
    this.t = t;
  }

  public map<V>(f: (v: T) => V): Option<V> {
    if (this.t) {
      return Option.of(f(this.t));
    }
    return Option.of();
  }

  public filter(f: (v: T) => boolean): Option<T> {
    if (this.t && f(this.t)) {
      return this;
    }
    return Option.of();
  }

  public get(fb: () => T): T {
    if (this.t) {
      return this.t;
    }
    return fb();
  }

  public or(fb: () => Option<T>): Option<T> {
    if (this.t) {
      return this;
    }
    return fb();
  }

  public isEmpty(): boolean {
    return !this.t;

  }
}

function* iterate<T>(map: { [id: string]: T }): Iterator<T> {
  let keys = Object.keys(map);
  for (let i = 0; i < keys.length; i++) {
    yield map[keys[i]];
  }
}

function* iterateMap<T>(map: { [id: string]: T }): Iterator<{ key: string, value: T }> {
  let keys = Object.keys(map);
  for (let i = 0; i < keys.length; i++) {
    yield {
      key: keys[i],
      value: map[keys[i]]
    };
  }
}
