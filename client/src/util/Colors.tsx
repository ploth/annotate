export function ColorCount(no: number): string {
  const first = no % 2;
  const second = Math.round(no / 2) % 2;
  const third = Math.round(no / 4) % 2;
  return `#${[third, second, first].map(noToHex).join('')}`;
}

function noToHex(no: number): string {
  switch (no) {
    case 0:
      return '00';
    case 1:
      return 'AA';
    default:
  }
  return '';
}
