import { ReducerFactory } from '../util/Reducers';
import { Actions, OwnProps, Props, State } from './ScoreModel';
import { connect, Dispatch } from 'react-redux';
import { RootState } from '../index';
import { Scores } from '../api/Scores';
import axios from 'axios';
import { Component } from './ScoreView';
import { Stats } from '../api/Stats';

const actions = new ReducerFactory<State, OwnProps>();

const RequestScores = actions.createVoidAction((state, action) => {
  axios.get('/v1/scores')
    .then(r => r.data as Scores)
    .then(s => HandleScores.create(action.dispatch, action.props)(s));
  return state;
});

const HandleScores = actions.createAction<Scores>((s, a) => {
  return {
    ...s,
    scores: a.value
  };
});

const RequestStats = actions.createVoidAction((state, a) => {
  axios.get('/v1/stats')
    .then(r => r.data as Stats)
    .then(s => HandleStats.create(a.dispatch, a.props)(s));
  return state;
});

const HandleStats = actions.createAction<Stats>((s, a) => ({
  ...s,
  stats: a.value
}));

const initialState: State = {
  scores: {},
  stats: {
    labeled: -1,
    remaining: -1
  }
};
export const Reducer = actions.build(initialState);

export const Score = connect(
  (rootState: RootState, ownProps: OwnProps): Props => ({
    state: rootState.scores
  }),
  (dispatch: Dispatch<{}>, ownProps: OwnProps): Actions => ({
    onLoad: () => {
      RequestScores.create(dispatch, ownProps)();
      RequestStats.create(dispatch, ownProps)();
    }
  })
)(Component);
