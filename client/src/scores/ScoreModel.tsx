import { Scores } from '../api/Scores';
import { Stats } from '../api/Stats';

export interface OwnProps {

}

export interface State {
  scores: Scores
  stats: Stats
}

export interface Actions {
  onLoad: () => void
}

export interface Props extends OwnProps {
  state: State
}
