import { Actions, Props } from './ScoreModel';
import { Button, Jumbotron, Table } from 'react-bootstrap';
import * as React from 'react';
import { Lifecycle } from '../Lifecycle';
import { Stream } from '../util/Types';

export const Component = (props: Props & Actions) => (
  <Jumbotron>
    <Lifecycle
      componentDidMount={props.onLoad}
    />
    <h3>
      Highscores
    </h3>
    <p>
      {props.state.stats.labeled > 0
        ? `Thank you for labeling ${props.state.stats.labeled} images!`
        : 'loading...'
      }
      {props.state.stats.remaining > 0
        ? ` Only ${props.state.stats.remaining} to go!`
        : null
      }
    </p>
    <Table>
      <thead>
      <tr>
        <th>Name</th>
        <th>Score</th>
      </tr>
      </thead>
      <tbody>
      {Stream
        .ofMap(props.state.scores)
        .toArray()
        .sort((a, b) => b.value - a.value)
        .map(v => (
          <tr key={v.key}>
            <td>{v.key}</td>
            <td>{round(v.value, 2)}</td>
          </tr>
        ))
      }
      </tbody>
    </Table>
    <Button
      onClick={e => props.onLoad()}
    >
      Refresh
    </Button>
  </Jumbotron>
);

const round = (v: number, digits: number) =>
  Math.round(v * Math.pow(10, digits)) / Math.pow(10, digits);
