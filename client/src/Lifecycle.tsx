import * as React from 'react';

export class Lifecycle extends React.Component {
  props: {
    componentDidMount: () => void;
  };

  componentDidMount() {
    this.props.componentDidMount();
  }

  render() {
    return null;
  }
}
