FROM node:alpine
WORKDIR /opt/annotate
COPY server/package.json .
RUN npm install
COPY server/build ./build
COPY client/build ./public
EXPOSE 3001
ENV DATA_DIR=/opt/annotate/data
CMD [ "npm", "start" ]
