import * as express from 'express';
import * as bodyParser from 'body-parser';
import { Auth } from './api/Auth';
import { Image } from './api/Image';
import { Label } from './api/Label';
import { Scores } from './api/Scores';
import { Statics } from './api/Statics';
import * as log4js from 'log4js';
import * as path from 'path';
import { getDataDir } from './business/Util';

log4js.configure({
                   appenders: { file: { type: 'file', filename: path.join(getDataDir(), 'annotate.log') } },
                   categories: { default: { appenders: ['file'], level: 'info' } }
                 });

class App {
  public express: express.Application;
  private image = new Image();
  private label = new Label();
  private score = new Scores();
  private statics = new Statics();
  private auth = new Auth();

  constructor() {
    this.express = express();
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({extended: true}));
    this.express.use(express.json());
    this.express.use(express.urlencoded());
    this.mountRoutes();
  }

  private mountRoutes(): void {
    this.express.post('/v1/auth', this.auth.postAuth);
    this.express.get('/v1/auth/clientId', this.auth.getClientID);

    this.express.post('/v1/image', this.image.postV1);
    this.express.get('/v1/image', this.image.getV1);
    this.express.get('/v1/stats', this.image.getStats);
    this.express.get('/v1/labels', this.label.getV1);
    this.express.get('/v1/scores', this.score.get);

    this.express.get('/v2/labels', this.label.getV2);
    this.express.get('/v2/image', this.image.getV2);
    this.express.post('/v2/image', this.image.postV2);

    this.express.get('/v3/labels', this.label.getV3);

    this.express.use(this.statics.statics);
    this.express.use(this.statics.images);
  }
}

export default new App().express;
