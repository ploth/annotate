import { Label } from '../api/model/Image';
import { getDataDir } from './Util';
import { AnnotationService } from '../persistence/AnnotationService';
import { Stats } from '../api/model/Stats';

export class LabelService {
  public static instance = new LabelService();
  private dataDir: string;
  private label: AnnotationService<Label>;

  getImage(url?: string): Promise<Label> {
    if (url) {
      return this.label.get(url).then( fixUrl );
    } else {
        return this.label.getRandom().then( fixUrl );
    }
  }

  set(label: Label): Promise<{}> {
    return this.label.set(label.url, label);
  }

  getDataDir(): string {
    return this.dataDir;
  }

  getStats(): Promise<Stats> {
    return this.label.getStats();
  }

  private constructor() {
    this.dataDir = getDataDir();
    console.log(`Using ${this.dataDir}`);

    this.label = new AnnotationService<Label>(key => ({url: key, boxes: []}));
  }
}

function fixUrl(tuple: [string, Label]): Label {
    return {
        ...tuple[1],
        url: tuple[0]
    };
}