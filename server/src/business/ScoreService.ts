import { Scores } from '../api/model/Scores';
import { getDataDir } from './Util';
import * as path from 'path';
import { JsonService, Migratable, Migration } from '../persistence/JsonService';
import { ScoresV2 } from '../persistence/Model';
import { Stream } from '../util/Types';
import { Label } from '../api/model/Image';

const initialScores: Scores = {};

const migrations: Migration<Migratable>[] = [
  {
    key: 'v2',
    apply: (v1: Scores): ScoresV2 => {
      delete v1.migrations;
      return {
        migrations: [],
        scores: v1,
        sum: Stream.ofMap(v1)
          .map(entry => entry.value)
          .fold<number>((a, b) => a + b, 0)
      };
    }
  }
];

export class ScoreService {
  public static instance = new ScoreService();
  private scores: JsonService<Scores, ScoresV2>;

  getScores(): Scores {
    return this.scores.get().scores;
  }

  addScore(user: string, label: Label): void {
    const linePoints = label.lines ? label.lines.length : 0;
    const boxPoints = label.boxes.length;
    const points = linePoints * 1.5 + boxPoints * 1.5 + 1;

    this.scores.update((c: ScoresV2) => {
      if (c.scores[user]) {
        c.scores[user] += points;
      } else {
        c.scores[user] = points;
      }
      c.sum += points;
      return c;
    });
  }

  private constructor() {
    this.scores = new JsonService<Scores, ScoresV2>(
      path.join(getDataDir(), 'scores.json'),
      initialScores,
      migrations);
  }
}
