import { Label } from '../api/model/Image';
import { Migratable } from './JsonService';
import { LabelsV2 } from '../api/model/LabelsV2';
import { LabelsV3 } from '../api/model/LabelsV3';
import { Scores } from '../api/model/Scores';

export interface ConfigV1 {
  labels: string[];
}

export interface ConfigV2 extends Migratable {
  labels: LabelsV2;
}

export interface ConfigV3 extends Migratable {
  labels: LabelsV3;
}

export interface ConfigV4 extends ConfigV3 {
  inflation: number;
  auth: AuthConfig;
}

export interface ConfigV5 extends ConfigV3 {
  auth: AuthConfig;
}

export interface AuthConfig {
  clientID: string;
  secret: string;
}

export interface LabelData {
  data: {
    [key: string]: Label;
  };
}

export interface ScoresV2 extends Migratable {
  scores: Scores;
  sum: number;
}
