import * as fs from 'fs';
import * as path from 'path';
import { getDataDir } from '../business/Util';
import Lock from '../util/Locks';
import { Stats } from '../api/model/Stats';

class AnnotationIndex {
  private annotated: Set<string>;
  private unannotated: Set<string>;
  private dataDir: string;
  private imageSuffix: string;

  constructor(dataDir: string, imageSuffix: string) {
    this.dataDir = dataDir;
    this.imageSuffix = imageSuffix;
    this.annotated = new Set();
    this.unannotated = new Set();
    walk(dataDir, file => this.updateFile(file));
    fs.watch(dataDir, (e: string, file: string) => {
      this.updateFile(file);
    });
  }

  public update(key: string): void {
    this.updateFile(path.join(this.dataDir, key));
  }

  private updateFile(file: string): void {
    if (fs.existsSync(file)) {
      if (file.endsWith(this.imageSuffix)) {
        const key = path.relative(this.dataDir, file);
        if (fs.existsSync(jsonName(file))) {
          this.annotated.add(key);
          this.unannotated.delete(key);
        } else {
          this.unannotated.add(key);
          this.annotated.delete(key);
        }
      }
    }
  }

  public getRandomAnnotated(): string | null {
    if (this.annotated.size > 0) {
      let index = Math.round(Math.random() * (this.annotated.size - 1));
      return Array.from(this.annotated)[index];
    }
    return null;
  }

  public getRandomUnannotated(): string | null {
    if (this.unannotated.size > 0) {
      let index = Math.round(Math.random() * (this.unannotated.size - 1));
      return Array.from(this.unannotated)[index];
    }
    return null;
  }

  public has(key: string) {
    return this.annotated.has(key) || this.unannotated.has(key);
  }

  public getStats(): Stats {
    const stats = {
      labeled: this.annotated.size,
      remaining: this.unannotated.size
    };
    return stats;
  }
}

export class AnnotationService<T> {
  private defaultContent: (key: string) => T;
  private dataDir: string;
  private fileExt: string;
  private index: AnnotationIndex;

  constructor(defaultContent: (key: string) => T, fileExt: string = 'png') {
    this.dataDir = getDataDir();
    this.fileExt = fileExt;
    this.defaultContent = defaultContent;
    this.index = new AnnotationIndex(this.dataDir, this.fileExt);
  }

  get(key: string): Promise<[string, T]> {
    return new Promise((resolve, reject) => {
      if (!this.index.has(key)) {
        reject('Unknown key.');
        return;
      }
      resolve([key, this.getOrDefault(key)]);
    });
  }

  set(key: string, value: T): Promise<{}> {
    return new Promise((resolve, reject) => {
      if (!this.index.has(key)) {
        reject('Unknown key.');
        return;
      }
      const file = jsonName(path.join(this.dataDir, key));
      new Lock(`${file}.lck`).lock(() => {
        fs.writeFileSync(file, JSON.stringify(value));
        this.index.update(key);
        resolve();
      });
    });
  }

  getStats(): Promise<Stats> {
    return new Promise((res, rej) => {
      res(this.index.getStats());
    });
  }

    getRandom(): Promise<[string, T]> {
      return new Promise((resolve, reject) => {
            const unannotated = this.index.getRandomUnannotated();
            if (unannotated) {
                resolve([unannotated, this.defaultContent(unannotated)]);
                return;
            }
            const annotated = this.index.getRandomAnnotated();
            if (annotated) {
                const file = jsonName(path.join(this.dataDir, annotated));
                resolve([annotated, JSON.parse(fs.readFileSync(file, 'UTF-8'))]);
                return;
            }
            reject('No Keys available.');
        });
    }

    private getOrDefault(key: string): T {
        let json = jsonName(path.join(this.dataDir, key));
        if (fs.existsSync(json)) {
            return JSON.parse(fs.readFileSync(json, 'utf-8'));
        }
        return this.defaultContent(key);
    }

}

function jsonName(key: string) {
    return `${key}.json`;
}

function walk(filePath: string, cb: ((n: string) => void)): void {
    for (let file of fs.readdirSync(filePath)) {
        let fullPath = path.join(filePath, file);
        const stats = fs.lstatSync(fullPath);
        if (stats.isDirectory()) {
            walk(fullPath, cb);
        }
        if (stats.isFile()) {
            cb(fullPath);
        }
    }
}
