import * as express from 'express';
import { ConfigService } from '../business/ConfigService';
import { LabelsV2 } from './model/LabelsV2';

export class Label {
  protected labelService = ConfigService.instance;

  public getV1 = (req: express.Request, res: express.Response) => {
    res.json(this.labelService.getLabels().boxes);
  }

  public getV2 = (req: express.Request, res: express.Response) => {
    const labels = this.labelService.getLabels();
    const data: LabelsV2 = {
      images: labels.images,
      boxes: labels.boxes
    };
    res.json(data);
  }

  public getV3 = (req: express.Request, res: express.Response) => {
    res.json(this.labelService.getLabels());
  }
}
